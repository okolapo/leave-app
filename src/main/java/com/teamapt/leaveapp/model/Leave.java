package com.teamapt.leaveapp.model;

import com.teamapt.makerchecker.annonations.MakerCheckerGroupId;
import com.teamapt.makerchecker.model.MakerCheckerEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table (name = "leaves")
@MakerCheckerGroupId("leave")
public class Leave implements MakerCheckerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String startDate;

    private String endDate;

    private String reason;

    private Boolean dirty = false;

    private Boolean approved = false;

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public Boolean getDirty() {
        return dirty;
    }

    public void setDirty(Boolean dirty) {
        this.dirty = dirty;
    }

    @Override
    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }
}
