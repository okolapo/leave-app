package com.teamapt.leaveapp.controller;

import com.teamapt.leaveapp.model.Leave;
import com.teamapt.leaveapp.service.LeaveService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/leave")
public class LeaveController {

    private final LeaveService leaveService;

    public LeaveController(LeaveService leaveService) {
        this.leaveService = leaveService;
    }

    @RequestMapping(value = "/request-leave", method = RequestMethod.POST)
    public @ResponseBody
    Leave requestLeave(@RequestBody Leave leave, @RequestParam("name") String initiator) {
        return leaveService.requestLeave(leave, initiator);
    }

    @RequestMapping(value = "/update-leave", method = RequestMethod.PUT)
    public @ResponseBody
    Leave updateLeave(@RequestBody Leave leave, @RequestParam("name") String initiator) {
        return leaveService.updateLeave(leave, initiator);
    }

    @RequestMapping(value = "/delete-leave/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    Leave deleteLeave(@PathVariable Long id, @RequestParam("name") String initiator) {
        return leaveService.deleteLeave(id, initiator);
    }

    @RequestMapping(value = "/fetch-leaves", method = RequestMethod.GET)
    public @ResponseBody
    List<Leave> fetchLeaves() {
        return leaveService.fetchLeaves();
    }
}
