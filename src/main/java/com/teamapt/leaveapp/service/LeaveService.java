package com.teamapt.leaveapp.service;

import com.teamapt.leaveapp.model.Leave;
import com.teamapt.leaveapp.repository.LeaveRepository;
import com.teamapt.makerchecker.annonations.ActorName;
import com.teamapt.makerchecker.annonations.EnableMakerChecker;
import com.teamapt.makerchecker.annonations.TargetEntity;
import com.teamapt.makerchecker.enums.InitiationType;
import com.teamapt.makerchecker.enums.RequestType;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LeaveService {

    private final LeaveRepository leaveRepository;

    public LeaveService(LeaveRepository leaveRepository) {
        this.leaveRepository = leaveRepository;
    }

    @EnableMakerChecker(type = RequestType.CREATE)
    public Leave requestLeave(@TargetEntity(type = InitiationType.SINGLE) Leave leave, @ActorName String initiator) {
        return leaveRepository.save(leave);
    }

    @EnableMakerChecker(type = RequestType.UPDATE)
    public Leave updateLeave(@TargetEntity(type = InitiationType.SINGLE) Leave leave, @ActorName String initiator) {
        return leaveRepository.save(leave);
    }

    @EnableMakerChecker(type = RequestType.DELETE)
    public Leave deleteLeave(@TargetEntity(type = InitiationType.SINGLE) Long id, @ActorName String initiator) {
        Optional<Leave> leave = leaveRepository.findById(id);
        return leave.orElse(null);
    }

    public List<Leave> fetchLeaves() {
        return leaveRepository.findAllByApproved(true);
    }
}
