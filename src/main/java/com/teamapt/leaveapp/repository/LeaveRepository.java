package com.teamapt.leaveapp.repository;

import com.teamapt.leaveapp.model.Leave;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LeaveRepository extends JpaRepository<Leave, Long> {
    List<Leave> findAllByApproved(boolean approved);
}
